import com.sun.org.apache.xpath.internal.operations.Bool;

import java.util.*;
import java.awt.event.*;

/**
 * --------------------------------------------------------
 * KeyboardApp.java:
 * Pig Latin Translator
 *
 * @author Steven Lin
 */

public class KeyboardApp extends StringUtilities{
    //--------------------------------------------------------

    /**
     * Creates a Frame and a KeyBoardApp.
     *
     * @param args String[]
     */
    public static void main(String args[]) {
        Scanner keyboard = new Scanner(System.in);
        Boolean wantsToType = true;

        while (wantsToType) {
            String input;
            System.out.println("Enter words to be translated(type quit to stop): ");
            input = keyboard.nextLine();
            if (input.equalsIgnoreCase("quit")) {
                wantsToType = false;
                return;
            }
            System.out.println(translate(input));
        }
    }
}
